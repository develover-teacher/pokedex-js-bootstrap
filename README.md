# pokedex-js-bootstrap

Ejemplo de código que usa JavaScript y Bootstrap para mostrar una lista de pokemons en una web responsive.

Fuente original: https://github.com/Helvette/pokedex

Mi aportación es que esta  versión permite buscar Pokemons por tipo (aparte de por nombre), usando la Restful PokeAPI: https://pokeapi.co/

